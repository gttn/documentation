# Git Workflow
We would like to try to follow the standard git workflow (pushes, pulls, feature/bugfix branches). 

An example model can be found [here](https://nvie.com/posts/a-successful-git-branching-model/).

# Development to Production Process

## Code (module)
Code development (of Drupal modules) can be executed with the following workflow
1. **Local:** Code can be edited in a feature branch and tested in an local development environment
2. **Development** Once the feature is tested on local, it can be merged (see [Code Review]() section below) to master (or perhaps a dev **branch**) on git, and then tested on *Development Server*
3. **Production** Once we are happy that the new feature/function works well in the site's environment, those changes can be pushed to the *Production Server*


*For reference:*
 - [GTTN Profile (Gitlab)](https://gitlab.com/gttn/gttn_profile)
 - [GTTN DR (Gitlab)](https://gitlab.com/gttn/gttn-dr)

## Data/Database Content

It would also be useful for the development server (both local and official) to have sample data in them to mimic the database state of the production. This can be handled by so-called Data Factories:

A [TripalTestSuite module](https://github.com/tripal/TripalTestSuite/blob/master/docs/source/factories.rst) exists for this purpose, and features the ability to load a database with fake/sample data based on some template. This can help us ensure that the module plays nicely with the data that may already exist.

# Code Review
Feature or bugfix branches will need to be merged into a master branch periodically.

There is currently no fixed plan for this. One suggestion that was made that, due to the time differences between the two main parties, code is pushed at end-of-day of one party and then reviewed by the other party.

Communication between the two teams might be useful/required in these cases. 