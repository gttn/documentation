# Documentation

This is where we will keep modules and other resources that will be useful for the development of the GTTN website

# Web Resources

Keep any links to Google spreadsheets or other shared resources in this area

[Metadata (Reference Data)](https://docs.google.com/spreadsheets/d/1-D5lyZuEZDnVxGXNqia997vm1Wpu9a7XcHPOQ_pQSq0/edit#gid=0)

[SPD Metadata](https://docs.google.com/spreadsheets/d/1lee0yKDzclWyTMFDiN2DIw5HWvB6GWJwJyQrR7x733g/edit#gid=0)

# Historical/Archive
Keep any past meeting notes here and include the date and a brief description

[June 6, 2019 - Skype between UConn and GTTN members (Google docs)](https://docs.google.com/document/d/1Y9Qq7q6o8RWAYMLq4PECr-IfpGr08k2paEFFGB1V_Yk/edit#)
- development best practices
- legal considerations about hosting data
- data storage plan for partner sites
