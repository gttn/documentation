# Files in this directory

* GTTN_SPD.dia
  - ER Diagram for basic Service Provider Database. Open with [Dia Diagram Editor](http://dia-installer.de/)
* GTTN_SPD.png
* - ER Diagram (rendered PNG)
